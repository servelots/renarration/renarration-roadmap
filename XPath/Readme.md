index.html imports myScript.js
    
    generates a prompt to get an html file / url of interest
    and displays it. 
    Which onclicking a line will provide pop up its xpath

xpath.html is a sample file that is used as a parameter file for index.html
This can be a simple file or a series of files, but we use any URL of an html file.

12/4/2023 tasks
[ ] take any url as and open it from index.html
[ ] on click display the xpath as popup and highlight the element at the xpath
[ ] the popup can take an alternate string and replace it at the element selected.


