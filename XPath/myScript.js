function getXPath(element) {
    if (element.id !== '')
      return '//*[@id="' + element.id + '"]';
    if (element === document.body)
      return element.tagName;

    var ix = 0;
    var siblings = element.parentNode.childNodes;
    for (var i = 0; i < siblings.length; i++) {
      var sibling = siblings[i];
      if (sibling === element)
        return  getXPath(element.parentNode) + '/' + element.tagName + '[' + (ix + 1) + ']';
      if (sibling.nodeType === 1 && sibling.tagName === element.tagName)
        ix++;
    }
}


window.addEventListener("DOMContentLoaded", (event) => {
  
  const el =  document.querySelector(".myDiv");
  // console.log(el)
  
  if (el) {
    
    el.addEventListener('click', function(event) {
      event.target.style.backgroundColor = "yellow";
      setTimeout(()=>{
        let val = prompt( `Please enter change text to this XPath: ${getXPath(event.target)} \n \n ${event.target.innerHTML}`, "");
        if(val){
          event.target.innerHTML = val;
          event.target.style.backgroundColor = "green";
        }
        
      },100)
        
      
    }, false);
  }
});