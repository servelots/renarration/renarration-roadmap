Roadmap will start from earlier work on renarration, charts out activities on revamping them, building simple tools and applications that helps develop a Web Annotation based Social Semantic Web platform with dWeb and Community Networks in mind.

See open.janastu.org/projects/renarration and open.janastu.org/projects/alipi for background information.

We can think of approaching it on 2 tracks:

1. revamping the earlier archived application at: alipi.us, restory.swtr.us and mitan.in/bcp/raika - all of them use the backend at http://y.a11y.in/web which has been made available here under the rennaration project https://gitlab.com/servelots/renarration/mitan-bcp-raika/-/tree/main/ya11yin

This will be a reference working implementation of what we have and need for the short term. An architecture and user documentation will be made available. Note that to make this functional, we need to re enable the swt-repositories to store and reuse ones annotations and renarrations. See alipi.us/dir to see how earlier the target urls and the annotation stores are collated.


2. Start afresh using newer browser utilities and libraries:

    2.1 To develop Addons, Bookmarlets and assistive tools to extract XPATH of a fragment from a page of interest, use it to construct a rugged deep link available for an annotation, collect the body (can be text or a link to a media fragment), and store it in a store of choice.
    2.2 To develop a rendering tool or two that utilises the annotations for a given URL and a choice of stores. This tool can respond to generic annotaions or renarration annotations (see W3C Web Annotation Model and https://www.w3.org/annotation/wiki/Use_Cases/Social_Semantic_web)
    2.3 Annotation can be passed as a message to someone on a social media network or posted as a micro blog.
    2.4 A service to render such a micro-blog
    2.5 A service that can use annotation repository in addition to rendering a micro blog annotation
    2.6 A document maker of a target URL and the document ID is used in the repo/store/message. Such a document will be a dweb document and can be replicated, archived and made available in internet independent networks. Such a document will also be available as a reference document if the content of the target URL changes.
    2.7 ..

There is also the possibility of merging the 2 tracks depending of how we see possibilities.

We can ofcourse imagine a day where current developers and the alumni connect and hack together.
